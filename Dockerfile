FROM launcher.gcr.io/google/ubuntu16_04 AS base

MAINTAINER IT Arch <aws-itarch@mscloud.emory.net>

ARG bb_auth_string
ENV BB_AUTH_STRING=$bb_auth_string
ENV CLOUDSDK_PYTHON="python3"
ARG CLOUD_SDK_VERSION=276.0.0
ENV CLOUD_SDK_VERSION=$CLOUD_SDK_VERSION

RUN apt-get -y update && apt-get -y install \
        wget \
        gcc \
        python3 \
        python3-dev \
        python3-setuptools \
        python3-pip \
        python-pip \
        ca-certificates \
        zip \
        jq \
        git \
        moreutils \
        software-properties-common \
        python-software-properties

# upgrade pip because it's the right thing to do (it also makes it so we can
# use pip instead of remembering to use pip3)
# pip2 needed for network tools
RUN pip2 install -U pip && \
    pip3 install -U pip && \
    pip3 install -U crcmod

# Install network tools
RUN pip2 install -U ncclient
WORKDIR /opt/rhedcloud
RUN git clone https://jimmykincaid@bitbucket.org/jbkinca/emory-aws-vpn-csr1000v-lab.git

# Setup Google Cloud SDK
RUN mkdir -p /builder && \
    wget -qO- https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-${CLOUD_SDK_VERSION}-linux-x86_64.tar.gz | tar zxv -C /builder && \
    /builder/google-cloud-sdk/install.sh --usage-reporting=false \
        --bash-completion=false \
        --disable-installation-options \
        --additional-components beta

RUN curl -fLo /usr/local/bin/kubectl https://storage.googleapis.com/kubernetes-release/release/v1.18.0/bin/linux/amd64/kubectl && \
	chmod a+x /usr/local/bin/kubectl
RUN curl -s "https://raw.githubusercontent.com/kubernetes-sigs/kustomize/master/hack/install_kustomize.sh" | bash && \
	mv kustomize /usr/local/bin/

WORKDIR /opt/rhedcloud
RUN git clone https://${BB_AUTH_STRING}@bitbucket.org/rhedcloud/rhedcloud-gcp-pipeline-scripts.git && \
    cd rhedcloud-gcp-pipeline-scripts && \
    pip install -Ur requirements-dev.txt

RUN BB_TEAM=rhedcloud ./rhedcloud-gcp-pipeline-scripts/download_artifact.sh rhedcloud-pytest-plugins rhedcloud-pytest-plugins.latest.zip && \
    cd rhedcloud-pytest-plugins && \
    pip install -e '.[gcp]'

ENV GCLOUD_HOME /builder/google-cloud-sdk/bin
ENV PATH /opt/rhedcloud/rhedcloud-gcp-pipeline-scripts:$GCLOUD_HOME:$PATH

# RUN BB_TEAM=rhedcloud ./rhedcloud-gcp-pipeline-scripts/download_artifact.sh rhedcloud-aws-python-testutils rhedcloud-aws-python-testutils.latest.zip
# ENV PYTHONPATH /opt/rhedcloud/rhedcloud-aws-python-testutils:$PYTHONPATH
