# Emory Docker Image For AWS Testing

This repository contains information for building a custom Docker image that
includes dependencies common to many test pipelines. Such dependencies include:

* System packages
  * `zip`
* Python packages
  * `boto3`
  * `pytest`
  * `pytest-mock`
  * `pytest-xdist`
  * `pytest-verbose-parametrize`
* Emory packages
  * `emory-aws-pipeline-scripts`
  * `emory-aws-python-testutils`

The scripts from `emory-aws-pipeline-scripts` are places on the `PATH` for easy
access when using the Docker image. Likewise, the utilities in the
`emory-aws-python-testutils` repo are placed on the `PYTHONPATH` to be
immediately used by any test scripts within a Docker container based on this
image.

## Building The Image

To build the Docker image locally, the following environment variable must be
defined:

* `BB_AUTH_STRING`: Your Bitbucket username and password in the form of
  `username:password`. This is required to clone the
  `emory-aws-pipeline-scripts` repository, in order to access the latest copy
  of the scripts.

Once you have defined that environment variable, you may proceed to build the
Docker image using the following command:

    $ make build

## Pushing The Image

If you wish to push your locally-built image to the Docker Hub, you must define
the following environment variables:

* `DOCKER_HUB_USERNAME`: your username to the Docker Hub
* `DOCKER_HUB_PASSWORD`: the password associated with your Docker Hub account

Docker Hub requires authentication in order to push any images, thus your
account requires permission to Emory's organization on Docker Hub. Once you
have defined those environment variables, you may proceed to push the built
image using the following command:

    $ make push
